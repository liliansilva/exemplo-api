## Exemplo de acesso a API da Central Skyynet

Esse exemplo simples contém o necessário para consumir a API atráves do Ionic/Angular.

<br />

# Crie um app com Ionic versão 2+ (neste exemplo usando Ionic v4)

<pre>ionic start exemplo sidemenu --type=angular</pre>

# Navegue para a pasta do app criado

<pre>cd exemplo/</pre>

# Crie um service para a API

<pre>ionic g service services/api</pre>

# Em app/app.module.ts, adicione o import abaixo

<pre>import { HttpClientModule } from '@angular/common/http';</pre>

# Em seguida coloque o `HttpClientModule` na lista de imports do módulo, o código da app.module.ts deve ficar assim:

<pre>
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
</pre>

# No arquivo services/api.service.ts coloque o código:

<pre>import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  // essa variavel pode estar no seu arquivo environments/environment.ts
  endereco = 'https://api.skyynet.com.br/api.cfm';
  constructor(private http: HttpClient) {}

  // exemplo listando os dispositivos
  listarDispositivos(chave: string) {
    return this.http.get(
      this.endereco + `?chave=${chave}&comando=listardispositivos`
    );
  }

  // exemplo enviando um comando
  reiniciarCentral(chave) {
    return this.http.get(`${this.endereco}?comando=reiniciar&chave=${chave}`);
  }
}

</pre>

# No arquivo pages/home/home.page.html adicione o código

```html
<ion-content [fullscreen]="false">
  <ion-header collapse="condense">
    <ion-toolbar>
      <ion-title size="large">Blank</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-button (click)="reiniciar()" expand="block" fill="clear" shape="round">
    Reiniciar
  </ion-button>

  <div id="container">
    <ul>
      <li *ngFor="let dispositivo of dispositivos">
        <p>
          O dispositivo {{dispositivo.nome}} está ativo? {{dispositivo.ativo}}
        </p>
      </li>
    </ul>
  </div>
</ion-content>
```

# No arquivo pages/home/home.page.ts

<pre>
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  // a chave nao deve ser hard coded, está aqui apenas como exemplo
  chave = '00000000-0000-0000-0000000000000000';
  dispositivos: any = [];
  constructor(private api: ApiService) {}

  ngOnInit() {
    this.listarDispositivos();
  }

  listarDispositivos() {
    this.dispositivos = [];

    this.api.listarDispositivos(this.chave).subscribe(
      (data) => {
        this.dispositivos = data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  reiniciar() {
    this.api.reiniciarCentral(this.chave).subscribe(() => {
      console.log('comando enviado com sucesso!');
    });
  }
}

</pre>
