import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  // a chave nao deve ser hard coded, está aqui apenas como exemplo
  chave = '00000000-0000-0000-0000000000000000';
  dispositivos: any = [];
  constructor(private api: ApiService) {}

  ngOnInit() {
    this.listarDispositivos();
  }

  listarDispositivos() {
    this.dispositivos = [];

    this.api.listarDispositivos(this.chave).subscribe(
      (data) => {
        this.dispositivos = data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  reiniciar() {
    this.api.reiniciarCentral(this.chave).subscribe(() => {
      console.log('comando enviado com sucesso!');
    });
  }
}
