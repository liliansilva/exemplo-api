import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  // essa variavel pode estar no seu arquivo environments/environment.ts
  endereco = 'https://api.skyynet.com.br/api.cfm';
  constructor(private http: HttpClient) {}

  // exemplo listando os dispositivos
  listarDispositivos(chave: string) {
    return this.http.get(
      this.endereco + `?chave=${chave}&comando=listardispositivos`
    );
  }

  //exemplo enviando um comando
  reiniciarCentral(chave) {
    return this.http.get(`${this.endereco}?comando=reiniciar&chave=${chave}`);
  }
}
